#%%
import numpy as np
import random
import cv2
import os
from imutils import paths
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score

import tensorflow as tf
import pandas as pd
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import SGD
from tensorflow.keras import backend as K
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
import joblib

from utils import *
#%%
config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

train_df = pd.read_csv("./ECG-Dataset/mitbih_train.csv", header=None)
test_df = pd.read_csv("./ECG-Dataset/mitbih_test.csv", header=None)

print(train_df.shape)
print(test_df.shape)

train_x = np.array(train_df[train_df.columns[0:-1]], dtype=np.float32)
train_y = np.array(train_df[train_df.columns[-1:]], dtype=np.float32)

test_x = np.array(train_df[test_df.columns[0:-1]], dtype=np.float32)
test_y = np.array(train_df[test_df.columns[-1:]], dtype=np.float32)

print("print train set is : x = {} y = {}".format(train_x.shape, train_y.shape))
print("print test set is : x = {} y = {}".format(test_x.shape, test_y.shape))

# Return difference array
def return_diff_array_table(array, dur):
  for idx in range(array.shape[1]-dur):
    before_col = array[:,idx]
    after_col = array[:,idx+dur]
    new_col = ((after_col - before_col)+1)/2
    new_col = new_col.reshape(-1,1)
    if idx == 0:
      new_table = new_col
    else :
      new_table = np.concatenate((new_table, new_col), axis=1)
#For concat add zero padding
  padding_array = np.zeros(shape=(array.shape[0],dur))
  new_table = np.concatenate((padding_array, new_table), axis=1)
  return new_table
#Concat
def return_merge_diff_table(df, diff_dur):
  fin_table = df.reshape(-1,187,1,1)
  for dur in diff_dur:
    temp_table = return_diff_array_table(df, dur)
    fin_table = np.concatenate((fin_table, temp_table.reshape(-1,187,1,1)), axis=2)
  return fin_table

#Use "stratify" option
X_train, X_test, y_train, y_test = train_test_split(train_x, train_y, test_size=0.2, stratify=train_y)

#%%
#Add Data
X_train = return_merge_diff_table(df=X_train, diff_dur=[1])
X_test = return_merge_diff_table(df=X_test, diff_dur=[1])
# %%
#create clients
clients = create_clients(X_train, y_train, num_clients=3, initial='client')

#process and batch the training data for each client
clients_batched = dict()
for (client_name, data) in clients.items():
    clients_batched[client_name] = batch_data(data)
    
#process and batch the test set  
test_batched = tf.data.Dataset.from_tensor_slices((X_test, y_test)).batch(len(y_test))

comms_round = 10
    
#create optimizer
learning_rate = 0.01 
loss='categorical_crossentropy'
metrics = ['accuracy']
optimizer = SGD(lr=learning_rate, 
                decay=learning_rate / comms_round, 
                momentum=0.9
               ) 

#initialize global model
smlp_global = SimpleMLP()
global_model = smlp_global.build()
# %%
#commence global training loop

for comm_round in range(comms_round):
  print("COMMAN ROUND: " + str(comm_round))
  # get the global model's weights - will serve as the initial weights for all local models
  global_weights = global_model.get_weights()
  
  #initial list to collect local model weights after scalling
  scaled_local_weight_list = list()

  #randomize client data - using keys
  client_names= list(clients_batched.keys())
  random.shuffle(client_names)
  
  #loop through each client and create new local model
  for client in client_names:
      print(client)
      smlp_local = SimpleMLP()
      local_model = smlp_local.build()
      
      #set local model weight to the weight of the global model
      local_model.set_weights(global_weights)
      
      #fit local model with client's data
      local_model.fit(clients_batched[client], epochs=5,batch_size=1024, validation_data=(X_test,y_test))
      
      #scale the model weights and add to list
      scaling_factor = weight_scalling_factor(clients_batched, client)
      scaled_weights = scale_model_weights(local_model.get_weights(), scaling_factor)
      scaled_local_weight_list.append(scaled_weights)
      
      #clear session to free memory after each communication round
      K.clear_session()
      
  #to get the average over all the local model, we simply take the sum of the scaled weights
  average_weights = sum_scaled_weights(scaled_local_weight_list)
  
  #update global model 
  global_model.set_weights(average_weights)
  #test global model and print out metrics after each communications round
  global_model.evaluate(X_test, y_test)
# %%
# global_model = smlp_global.build()
# global_model.compile(
#               optimizer=optimizer,
#               loss=loss,
#               metrics=metrics)
filename = 'finalized_model.sav'
joblib.dump(global_model, filename)
global_model.evaluate(X_test, y_test)
# %%
loaded_model = joblib.load(filename)
result = global_model.predict(X_test)
result_ = np.array([])
for res in result:
  temp = 0
  for i in range(len(res) - 1):
    if res[i+1] > res[temp]:
      temp = i + 1
  temp = np.array([temp])
  result_ = np.append(result_, [temp])
  break
np.reshape(result_, (17511,1))
from sklearn.metrics import precision_score
precision = precision_score(y_test, result_, labels=[0,1,2,3,4], average='micro')
print(precision)
# %%
for i in result_:
  print(i)

# %%
