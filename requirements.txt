imutils==0.5.4
numpy==1.20.1
opencv_python==4.5.5.62
pandas==1.2.4
scikit_learn==1.0.2
tensorflow==2.8.0
